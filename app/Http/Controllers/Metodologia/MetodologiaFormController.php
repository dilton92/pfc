<?php

namespace Entropia\Http\Controllers\Metodologia;

use Illuminate\Http\Request;
use Entropia\Http\Controllers\Controller;
use Entropia\Models\Metodologia;

class MetodologiaFormController extends Controller
{
    private $metodologia;

    public function __construct(Metodologia $metodologia)
    {
        //criação do objeto
        $this->metodologia = $metodologia;
    }

    public function index()
    {
        $registros = Metodologia::all();
        return view('admin.metodForm', compact('registros'));
    }

    public function salvar(Request $request)
    {
        //Solicitação dos dados do form
        $dataForm = $request->all();

        //Insere no banco
        $insert = Metodologia::create($dataForm);
        if( $insert ){
            return redirect()->route('metodologiasuper.listar');
            //return redirect()->route('metodologia.index');
        }else {
            return ('A metodologia não foi inserida');  
        }
    }


    //Edit
    public function editar($id){
        $registros = Metodologia::find($id);
        return view('admin.listaMetodSuper', compact('registros'));
        
    }

    //Atualização no Banco
    public function atualizar(Request $request, $id){

        //Solicitação dos dados do form
        $dataForm = $request->all();
        
        $update = Metodologia::find($id)->update($dataForm);

        if( $update ){
            return redirect()->route('metodologia.index');
        }else {
            return ('A metodologia não foi atualizada');
        }
    }

    public function deletar($id){

        Metodologia::find($id)->delete();
        return redirect()->route('metodologiasuper.listar');
    }
}
