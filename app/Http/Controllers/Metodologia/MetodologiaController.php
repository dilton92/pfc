<?php

namespace Entropia\Http\Controllers\Metodologia;

use Illuminate\Http\Request;
use Entropia\Http\Controllers\Controller;
use Entropia\Models\Metodologia;

class MetodologiaController extends Controller
{
    private $metodologia;

    public function __construct(Metodologia $metodologia)
    {
        //criação do objeto
        $this->metodologia = $metodologia;
    }

    public function index()
    {   
        //Captura dos dados da tabela "metodologias"
        $registros = Metodologia::all();

        return view('listaMetodologia', compact('registros'));
    }

    public function indexSuper(Request $registros){

        $registros = Metodologia::all();
        return view('admin.listaMetodSuper', compact('registros'));
    }

    
    
}
