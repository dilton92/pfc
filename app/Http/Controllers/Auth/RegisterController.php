<?php

namespace Entropia\Http\Controllers\Auth;

use Entropia\User;
use Entropia\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
   
    /*
    |------------------------------------------------- -------------------------
    |Registre o controlador
    |------------------------------------------------- -------------------------
    |
    |Este controlador lida com o registro de novos usuários, bem como com seus
    |validação e criação. Por padrão, este controlador usa um traço para
    |fornecer essa funcionalidade sem exigir qualquer código adicional.
    |
    */

    use RegistersUsers;

    /**
     * Onde redirecionar usuários após o registro.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Crie uma nova instância do controlador
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Obtenha um validador para uma solicitação de registro recebida.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'          =>      ['required', 'string', 'max:255'],
            'nascimento'    =>      ['required', 'string', 'max:255'],
            'cargo'         =>      ['required', 'string', 'max:255'],
            'cpf'           =>      ['required', 'cpf', 'max:255', 'unique:users'],
            'nomeusuario'   =>      ['required', 'string', 'max:255', 'unique:users'],
            'email'         =>      ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'      =>      ['required', 'string', 'min:8', 'confirmed'],
            'avatar'        =>      ['required', 'string', 'max:255'],

        ]);
        
    }

    /**
     * Crie uma nova instância do usuário após um registro válido.
     *
     * @param  array  $data
     * @return \Entropia\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name'          =>      $data['name'],
            'nascimento'    =>      $data['nascimento'],
            'cargo'         =>      $data['cargo'],
            'nomeusuario'   =>      $data['nomeusuario'],
            'cpf'           =>      $data['cpf'],
            'avatar'        =>      $data['avatar'],
            'email'         =>      $data['email'],
            'password'      =>      Hash::make($data['password']),
        ]);
    }
        
}
