<?php

namespace Entropia\Http\Controllers\Ouvidoria;

use Illuminate\Http\Request;
use Entropia\Http\Controllers\Controller;
use Entropia\Models\Ouvidoria;

class OuvidoriaController extends Controller
{
    private $ouvidoria;

    public function __construct(Ouvidoria $ouvidoria)
    {
        //criação do objeto
        $this->ouvidoria = $ouvidoria;
    }

    public function index()
    {
        $registros = Ouvidoria::all();
        return view('ouvidoria');
    }

    public function salvar(Request $request)
    {
        //Solicitação dos dados do form
        $dataForm = $request->all();

        //Insere no banco
        $insert = Ouvidoria::create($dataForm);
        if( $insert ){

            return redirect()->route('ouvidoria');
            //return redirect()->route('ouvidoria.index');
        }else {
            
            return ('A ouvidoria não foi inserida');  
        }
    }
}
