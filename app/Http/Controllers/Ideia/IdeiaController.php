<?php

namespace Entropia\Http\Controllers\Ideia;

use Illuminate\Http\Request;
use Entropia\Http\Controllers\Controller;
use Entropia\Models\Ideia;

class IdeiaController extends Controller
{
    private $ideia;

    public function __construct(Ideia $ideia)
    {
        //criação do objeto
        $this->ideia = $ideia;
    }

    public function index(Request $request)
    {
        $registros = Ideia::all();       
        //dd($registros);
        return view('ideia', compact('registros' ));
    }
    
    public function salvar(Request $request)
    {
        //Solicitação dos dados do form
        $dataForm = $request->all();

        if($request->hasFile('anexo')){
            $imagem = $request->file('anexo');
            $num = rand(1111,9999);
            $dir = "img/ideias";
            $ext = $imagem->guessClientExtension();
            $nomeImagem = "anexo_"."$num".".".$ext;
            $imagem->move($dir,$nomeImagem);
            $dataForm['anexo'] = $dir."/".$nomeImagem;
        }

        //Insere no banco
        $insert = Ideia::create($dataForm);
        
        if( $insert ){
            return redirect()->route('ideia');
        }else {
            return ('A ideia não foi inserida');  
        }
        
        
    }

    public function editar($id){
        $registro = Ideia::find($id);
        return view('ideiaEditar', compact('registro'));
        
    }

    public function atualizar(Request $request, $id){

        //Solicitação dos dados do form
        $dataForm = $request->all();        

        if($request->hasFile('anexo')){
            $imagem = $request->file('anexo');
            $num = rand(1111,9999);
            $dir = "img/ideias/";
            $ext = $imagem->guessClientExtension();
            $nomeImagem = "anexo_"."$num".".".$ext;
            $imagem->move($dir,$nomeImagem);
            $dataForm['anexo'] = $dir . $nomeImagem;
        }else {
            return ('Houve um problema e o arquivo não pôde ser inserido! Entre em contato com o suporte.');
        }

        $update = Ideia::find($id)->update($dataForm);

        if($update){
            return redirect()->route('ideia');
        }else{
            return "A ideia não pôde ser atualizada! Entre em contato com o suporte";
        }        
    }

    public function deletar($id){

        Ideia::find($id)->delete();
        return redirect()->route('ideia');
    }

}
