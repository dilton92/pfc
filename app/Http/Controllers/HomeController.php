<?php

namespace Entropia\Http\Controllers;

use Illuminate\Http\Request;
use Entropia\Http\Controllers\Auth;
use Entropia\Models\Ideia;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Ideia $ideias)
    {
        $ideias = Ideia::all();
        return view('home', compact('ideias'));
    }

    public function postar(Request $request, Ideia $post){

        $this->validate($request, [
            'descricao' => 'required|max:1000'            
        ]);
    
        $post = new Ideia();
        $post->titulo = $request['titulo'];
        $post->descricao = $request['descricao'];
        $post->beneficio = $request['beneficio'];
        $post->participante = $request['participante'];
        $post->id_user = $request['id_user'];
        $post->tipo_ideia = $request['tipo_ideia'];
        $request->user()->ideias()->save($post);

        return redirect()->route('home');
    }

}
