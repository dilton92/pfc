<?php

namespace Entropia\Http\Controllers\Campanha;

use Illuminate\Http\Request;
use Entropia\Http\Controllers\Controller;
use Entropia\Models\Campanha;

class CampanhaFormController extends Controller
{
    private $campanha;

    public function __construct(Campanha $campanha)
    {
        //criação do objeto
        $this->campanha = $campanha;
    }

    public function index()
    {
        $registros = Campanha::all();
        return view('admin.campanhaForm');
    }

    public function salvar(Request $request)
    {
        //Solicitação dos dados do form
        $dataForm = $request->all();

        //Insere no banco
        $insert = Campanha::create($dataForm);
        if( $insert ){
            return('insertou');
            //return redirect()->route('metodologia.index');
        }else {
            return ('A campanha não foi inserida');  
        }
    }
}
