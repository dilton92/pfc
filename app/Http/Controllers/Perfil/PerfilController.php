<?php

namespace Entropia\Http\Controllers\Perfil;

use Illuminate\Http\Request;
use Entropia\Http\Controllers\Controller;
use Entropia\Models\Perfil;

class PerfilController extends Controller
{
    public function index()
    {
        return view('perfil');
    }
}
