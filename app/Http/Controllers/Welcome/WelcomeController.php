<?php

namespace Entropia\Http\Controllers\Welcome;

use Illuminate\Http\Request;
use Entropia\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }
}
