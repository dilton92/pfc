<?php

namespace Entropia\Http\Controllers\Loja;

use Illuminate\Http\Request;
use Entropia\Http\Controllers\Controller;
use Entropia\Models\Loja;

class LojaController extends Controller
{
    public function index()
    {
        return view('loja');
    }
}
