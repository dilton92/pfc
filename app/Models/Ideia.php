<?php

namespace Entropia\Models;

use Illuminate\Database\Eloquent\Model;


class Ideia extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo', 
        'descricao', 
        'beneficio', 
        'participante', 
        'anexo',
        'quantidade_reacoes_ideia',
        'id_user',
        'tipo_ideia'
    ];

    public function user(){
        return $this->belongsTo('Entropia\User', 'id_user');
    }
}
