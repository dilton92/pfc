<?php

namespace Entropia\Models;

use Illuminate\Database\Eloquent\Model;

class Campanha extends Model
{
    protected $fillable = [
        'titulo','campanha', 'tema', 'descricao', 'beneficio', 'anexo', 'foto_camp'
    ];
}
