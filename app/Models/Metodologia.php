<?php

namespace Entropia\Models;

use Illuminate\Database\Eloquent\Model;

class Metodologia extends Model
{
    protected $fillable = [
        'titulo','definicao', 'descricao', 'objetivo', 
        //'anexo'
    ];
}
