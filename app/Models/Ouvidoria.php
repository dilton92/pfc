<?php

namespace Entropia\Models;

use Illuminate\Database\Eloquent\Model;

class Ouvidoria extends Model
{
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'assunto','setor', 'mensagem', //'anexo'
    ];
}
