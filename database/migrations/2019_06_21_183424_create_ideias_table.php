<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdeiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ideias', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->string('titulo');
            $table->string('descricao');
            $table->string('beneficio');
            $table->string('participante')->nullable();;
            $table->string('anexo')->nullable();
            $table->integer('quantidade_reacoes_ideia')->nullable();
            $table->integer('id_user');
            $table->integer('tipo_ideia');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ideias');
    }
}
