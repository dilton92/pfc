<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //O que chamar quando rodar o comando db:seed
        $this->call(UsersTableSeeder::class);
    }
}
