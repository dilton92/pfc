<?php

use Illuminate\Database\Seeder;
use Entropia\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'root',
            'nascimento'=> '12345620',
            'descricao'=> 'administrador',
            'cargo'=> '1',
            'nomeusuario'=> 'root',
            'cpf'=>'12345678900',
            'email' => 'root@root',
            'avatar'=> 'jjdsa',
            'password' => bcrypt('12345678'),
        ]);
    }
}
