<?php

//Welcome
Route::get('/', 'Welcome\WelcomeController@index')->name('welcome');
Auth::routes();

//Todos Dentro desse grupo precisam de Login pra serem acessado
Route::group(['middleware' => ['auth']], function () {
 

// Ideias
Route::get('/ideia', 'Ideia\IdeiaController@index')->name('ideia');
Route::post('/ideia/salvar', 'Ideia\IdeiaController@salvar')->name('ideia.salvar');
Route::get('/ideia-editar/{id}', 'Ideia\IdeiaController@editar')->name('ideia.editar');
Route::put('/ideia/atualizar', 'Ideia\IdeiaController@atualizar')->name('ideia.atualizar');
Route::get('/ideia-deletar/{id}', 'Ideia\IdeiaController@deletar')->name('ideia.deletar');


//Metodologia
Route::get('/metodologia', 'Metodologia\MetodologiaController@index')->name('metodologia');
Route::get('/metodologias', function(){ return view('listaMetodologia'); } )->name('metodologia.listar');


//MetodologiaFormSupervisor
Route::get('/metodologiaSuper', 'Metodologia\MetodologiaController@indexSuper')->name('metodologiasuper.listar');
Route::get('/metodologiaSuper/deletar/{id}', 'Metodologia\MetodologiaFormController@deletar')->name('metodologiasuper.deletar');
Route::get('/nova-metodologia', 'Metodologia\MetodologiaFormController@index')->name('metodologianova');
Route::post('/nova-metodologia/salvar', 'Metodologia\MetodologiaFormController@salvar')->name('metodologia.salvar');
Route::get('/nova-metodologia/editar/{id}', 'Metodologia\MetodologiaFormController@editar')->name('metodologia.editar');
Route::get('/nova-metodologia/atualizar', 'Metodologia\MetodologiaFormController@atualizar')->name('metodologia.atualizar');


// Home
Route::get('/home', 'HomeController@index')->name('home');
Route::post('home/post', 'HomeController@postar')->name('postar');


//Ouvidoria
Route::get('/ouvidoria', 'Ouvidoria\OuvidoriaController@index')->name('ouvidoria');
Route::post('/ouvidoria/salvar', 'Ouvidoria\OuvidoriaController@salvar')->name('ouvidoria.salvar');
Route::get('/ouvidoria/editar', 'Ouvidoria\OuvidoriaController@editar')->name('ouvidoria.editar');
Route::get('/ouvidoria/atualizar', 'Ouvidoria\OuvidoriaController@atualizar')->name('ouvidoria.atualizar');


//Campanha
Route::get('/campanha', 'Campanha\CampanhaController@index')->name('campanha');
Route::get('/campanha/campanhanova', 'Campanha\CampanhaFormController@index')->name('campanha.index');
Route::post('/campanha/campanhanova', 'Campanha\CampanhaFormController@salvar')->name('campanha.salvar');

Route::get('/cadastro', function(){
    return view('cadastro');
} );

//Perfil
Route::get('/perfil', 'Perfil\PerfilController@index')->name('perfil');


//Loja
Route::get('/loja', 'Loja\LojaController@index')->name('loja');


//
Route::get('/editar-campanha', function(){
    return view('/admin/campanhaEditar');
} );


Route::get('/campanha-add-ideia', function(){
    return view('/admin/addIdeia');
} );


//CapanhaSuper
Route::get('/campanhaSuper', function(){
    return view('/admin/listaCampSuper');
} );


Route::get('/avaliacao-campanhas', function(){
    return view('/admin/avaliacaoCampanhas');
} );


//Resto
Route::get('/premios', function(){
    return view('premios');
} );


Route::get('/comite', function(){
    return view('/admin/comite');
} );


Route::get('/validacao-ideias', function(){
    return view('/admin/validacao');
} );
});  

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
