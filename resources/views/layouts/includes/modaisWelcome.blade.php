<!-- Modals Avatars -->
      
      <!-- Avatar Modal 1 -->
      <div class="portfolio-modal mfp-hide" id="avatar1">
        <div class="portfolio-modal-dialog bg-white">
          <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
            <i class="fa fa-3x fa-times"></i>
          </a>
          <div class="container text-center">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <h2 class="text-secondary text-uppercase mb-0">Esteves</h2>
                <hr class="star-dark mb-5">
                <img class="img-fluid mb-5" src="{{asset('img/avatares/esteves-mini.png')}}" class="rounded mx-auto d-block">
                <p class="mb-5">Esse é Esteves, o personagem do Entropia. Suas características mais marcantes são: coragem, discernimento, confiança e comprometimento. Se for escolhido, será sua identidade no mais novo laboratório de inovações da sua empresa. Com base nas suas características, obterá seus primeiros pontos no sistema, venha já conhecer!</p>
                <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
                  <i class="fa fa-close"></i>
                FECHAR</a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Portfolio Modal 2 -->
      <div class="portfolio-modal mfp-hide" id="portfolio-modal-2">
        <div class="portfolio-modal-dialog bg-white">
          <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
            <i class="fa fa-3x fa-times"></i>
          </a>
          <div class="container text-center">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <h2 class="text-secondary text-uppercase mb-0">Muriel</h2>
                <hr class="star-dark mb-5">
                <img class="img-fluid mb-5" src="{{asset('img/avatares/muriel-mini.png')}}" class="rounded mx-auto d-block">
                <p class="mb-5">Esse é Muriel, o personagem do Entropia. Suas características mais marcantes são: flexibilidade, simpatia e empatia, Muriel também se destaca por ser bastante comunicativo. Se for escolhido, será sua identidade no mais novo laboratório de inovações da sua empresa. Com base nas suas características, obterá seus primeiros pontos no sistema, venha já conhecer!</p>
                <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
                  <i class="fa fa-close"></i>
                FECHAR</a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Portfolio Modal 3 -->
      <div class="portfolio-modal mfp-hide" id="portfolio-modal-3">
        <div class="portfolio-modal-dialog bg-white">
          <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
            <i class="fa fa-3x fa-times"></i>
          </a>
          <div class="container text-center">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <h2 class="text-secondary text-uppercase mb-0">Melo</h2>
                <hr class="star-dark mb-5">
                <img class="img-fluid mb-5" src="{{asset('img/avatares/melo-mini.png')}}" class="rounded mx-auto d-block">
                <p class="mb-5">Esse é Melo, o personagem do Entropia. Suas qualidades mais marcante é a sua lábia e o pensamento analítico e estratégico, também destaca-se por ser visionário. Se for escolhido, será sua identidade no novo laboratório de inovações da sua empresa. Com base nas suas características, obterá seus primeiros pontos no sistema, venha já conhecer!</p>
                <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
                  <i class="fa fa-close"></i>
                FECHAR</a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Portfolio Modal 4 -->
      <div class="portfolio-modal mfp-hide" id="portfolio-modal-4">
        <div class="portfolio-modal-dialog bg-white">
          <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
            <i class="fa fa-3x fa-times"></i>
          </a>
          <div class="container text-center">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <h2 class="text-secondary text-uppercase mb-0">Ariel</h2>
                <hr class="star-dark mb-5">
                <img class="img-fluid mb-5" src="{{asset('img/avatares/ariel-mini.png')}}" class="rounded mx-auto d-block">
                <p class="mb-5">Esse é Ariel, o personagem do Entropia. Suas qualidades mais marcantes são: pensamento crítico, paciência e persistência, também destaca-se por ser planejador. Se for escolhido, será sua identidade no mais novo laboratório de inovações da sua empresa. Com base nas suas características, obterá seus primeiros pontos no sistema, venha já conhecer!</p>
                <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
                  <i class="fa fa-close"></i>
                FECHAR</a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Portfolio Modal 5 -->
      <div class="portfolio-modal mfp-hide" id="portfolio-modal-5">
        <div class="portfolio-modal-dialog bg-white">
          <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
            <i class="fa fa-3x fa-times"></i>
          </a>
          <div class="container text-center">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <h2 class="text-secondary text-uppercase mb-0">César</h2>
                <hr class="star-dark mb-5">
                <img class="img-fluid mb-5" src="{{asset('img/avatares/Cesar-mini.png')}}" class="rounded mx-auto d-block">
                <p class="mb-5">Esse é César, o personagem do Entropia. Suas características mais marcantes são: flexibilidade, organização e multideterminação e também destaca-se por ser pró-ativo. Se for escolhido, será sua identidade no mais novo laboratório de inovações da sua empresa. Com base nas suas características, obterá seus primeiros pontos no sistema, venha já conhecer!
                </p>
                <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
                  <i class="fa fa-close"></i>
                FECHAR</a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Portfolio Modal 6 -->
      <div class="portfolio-modal mfp-hide" id="portfolio-modal-6">
        <div class="portfolio-modal-dialog bg-white">
          <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
            <i class="fa fa-3x fa-times"></i>
          </a>
          <div class="container text-center">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <h2 class="text-secondary text-uppercase mb-0">James</h2>
                <hr class="star-dark mb-5">
                <img class="img-fluid mb-5" src="{{asset('img/avatares/James.png')}}">
                <p class="mb-5">Esse é James, o personagem do Entropia. Suas características mais marcantes são: adaptação, criatividade, paciência e trabalho em equipe. Se for escolhido, será sua identidade no mais novo laboratório de inovações da sua empresa. Com base nas suas características, obterá seus primeiros pontos no sistema, venha já conhecer!</p>
                <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
                  <i class="fa fa-close"></i>
                FECHAR</a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Login -->
        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header" align="center">
                <div class="container text-center">
                  <div class="text-center" style="padding:50px 0">
                    <div class="logo">CONECTE-SE</div>
                    <!-- Main Form -->
                    <div class="login-form-1">
                      <form id="login-form" class="text-left" action="{{ route('login') }}" method="post">
                      @csrf
                        <div class="login-form-main-message"></div>
                        <div class="main-login-form">
                          <div class="login-group">
                          <div class="form-group">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-grou">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                         </div>
                         <button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
                       </div>
                       <div class="etc-login-form">
                        <p>É novo por aqui?<a href="{{ route('register') }}">Cadastre-se aqui</a></p>
                        <p>Esqueceu sua senha?<a href="{{ route('password.update')}}">Clique aqui</a></p>
                        <p>
                          <br>
                          <?php if (isset($_GET['erro'])) { ?>
                            <div class="alert alert-danger" role="alert">
                             Usuário e/ou senha inválidos.
                            </div>
                        <?php } ?>
                      </p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>