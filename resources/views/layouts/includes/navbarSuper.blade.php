<!--Navbar -->
<nav class="mb-4 navbar navbar-expand-lg navbar-dark unique-color-dark">
    <a class="navbar-brand" href="{{route('home')}}"><img src="img/logo/logo4.png"  width="100" height="50"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{route('ideia')}}"><i class="fa fa-lightbulb-o"></i>Ideias</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('ouvidoria') }}"><i class="fa fa-commenting-o"></i>Fale conosco</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('metodologiasuper.listar') }}"><i class="fa fa-book"></i>Metodologias</a>
            </li>
            
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bar-chart"></i>Central de Supervisão</a>
                <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="navbarDropdownMenuLink-4">
                    <a class="dropdown-item" href="premios.php">Prêmios da Loja</a>
                    <a class="dropdown-item" href="validacao.php">Validação de Ideias</a>
                    <a class="dropdown-item" href="comite.php">Comitê</a>
                    <a class="dropdown-item" href="campanha.php">Campanhas</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle"></i>
                    {{ Auth::user()->name }} 
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="navbarDropdownMenuLink-4">
                    <a class="dropdown-item" href="perfil.php">Meu Perfil</a>
                    <a class="dropdown-item" href="loja.php">Lojinha Virtual</a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </div>
</nav>
	<!--/.Navbar -->
