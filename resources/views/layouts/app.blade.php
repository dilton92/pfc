<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap/bootstrap.min.css') }}" id="bootstrap-css">        
        <link rel="stylesheet" type="text/css" href="{{ asset('css/padrao.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/cadastro.min.css') }}">        
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo/logo1.png') }}">
        <link type="text/css" rel="stylesheet" href="{{ asset('css/menu.min.css') }}">
        <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="{{ asset('css/premios.min.css') }}">
        

        <!-- Scripts -->
       
        

        @yield('script')
        

        <title>@yield('titulo')</title>

        
    </head>

    <body>
        @yield('content1')
            <main class="py-4">
                
                @yield('content')            
            </main>

        @yield('content2')
        
    </body>

    
    <script type="text/javascript" src="{{ asset('js/campanha.js') }}"></script>
    <script type="text/javascript" src="{{ asset('_js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('_js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/cadastro.js') }}"></script>
    <script src="{{ asset('_js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.4.1/js/mdb.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="js/datatables.min.js"></script>
    <script src="js/listagem.min.js"></script> 

    
    
    <script>
        $(document).ready(function () {

            var table = $('#datatable').DataTable();

            //Edição
            table.on('click', '.edit', function() {
                $tr = $(this).closest('tr');
                if($($tr).hasClass('child')) {
                    $tr = $tr.prev('.parent');
                }

                var data = table.row($tr).data();
                console.log(data);

                $('titulo').val(data[1]);
                $('descricao').val(data[2]);
                $('beneficio').val(data[3]);
                $('participante').val(data[4]);

                $('#listForm').attr(data[0]);
                $('#listModal').modal('show');
            });
        });
    </script>



</html>
