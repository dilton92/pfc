<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap/bootstrap.min.css') }}" id="bootstrap-css">
	<!-- Custom fonts for this template -->
	<link href="{{asset('css/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
	<!-- Plugin CSS -->
	<link href="{{asset('magnific-popup/magnific-popup.css')}}" rel="stylesheet" type="text/css">
	<!-- Custom styles for this template -->
	<link href="{{asset('css/index.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('css/login.css')}}">
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo/logo1.png') }}">
	<title>Entropia</title>
</head>

<body id="page-top">

@yield('content')

 <!-- Bootstrap core JavaScript -->
 <script src="{{asset('jquery/jquery.min.js')}}"></script>
  <script src="{{asset('js/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Plugin JavaScript -->
  <script src="{!!asset('jquery-easing/jquery.easing.min.js')!!}"></script>
  <script src="{{asset('magnific-popup/jquery.magnific-popup.min.js')}}"></script>

  <!-- Contact Form JavaScript -->
  <script src="{{asset('js/validacao-index.js')}}"></script>
  <script src="{{asset('js/contato-index.js')}}"></script>

  <!-- Custom scripts for this template -->
  <script src="{{asset('js/index.min.js')}}"></script>


</body>

</html>