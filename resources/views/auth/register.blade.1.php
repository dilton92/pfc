@extends('layouts.app')

@section('content')

<div class="container" id="cor">
    <img src="img/titulo/cadastra.png" class="rounded mx-auto d-block" id="foto">
    <div class="row justify-content-center">
        <div class="col-sm-5">
            <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data" >
                    @csrf
                <p>
                    <label for="reg_nome" class="sr-only">Nome</label>
                    <input id="name" type="text" placeholder="Nome Completo*" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                     @enderror
                </p>
                <p>
                    <label for="reg_data" class="sr-only">Data de Nascimento</label>
                    <input id="nascimento" type="text" placeholder="Nascimento*" class="data form-control @error('nascimento') is-invalid @enderror" name="nascimento" value="{{ old('nascimento') }}" required autocomplete="nascimento" autofocus>

                    @error('nascimento')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                     @enderror
                </p>
                <p>
                    <label for="reg_cargo" class="sr-only">Cargo</label>
                    <select class="form-control" id="reg_cargo" name="cargo">
                        <option value="0">Selecione seu cargo na empresa...</option>
                        <option value="1">Cargo 1</option>
                        <option value="2">Cargo 2</option>
                    </select>
                </p>
                <p>
                    <label for="reg_nome" class="sr-only">CPF</label>
                    <input id="cpf" type="text" placeholder="CPF*" class="form-control @error('cpf') is-invalid @enderror" name="cpf" value="{{ old('cpf') }}" required autocomplete="cpf" autofocus>

                    @if ($errors->has('cpf'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cpf') }}</strong>
                        </span>
                    @endif
                </p>
                <p>
                    <label for="reg_email" class="sr-only">Email</label>
                    <input id="email" type="email" placeholder="Email*" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </p>
                <p>
                    <label for="reg_usuario" class="sr-only">Nome de Usuário</label>
                    <input id="nomeusuario" type="text" placeholder="Nome de Usuário*" class="form-control @error('nomeusuario') is-invalid @enderror" name="nomeusuario" value="{{ old('nomeusuario') }}" required autocomplete="nomeusuario" autofocus>

                    @error('nomeusuario')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                     @enderror
                </p>
                <p>
                    <label for="reg_senha" class="sr-only">Senha</label>
                    <input id="password" type="password" placeholder="Senha*" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </p>
                <p>
                    <label for="reg_confirma_senha" class="sr-only">Confirmar senha</label>
                    <input id="password-confirm" type="password" placeholder="Confirme a senha*" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </p>

                <small class="form-text text-muted">(*)Campos Obrigatórios</small>
        </div>
        <div class="col-sm-6">
            <img src="img/titulo/avatar.png" class="rounded mx-auto d-block" alt="Escolha seu avatar">
            <center>
                <div id=avt>
                    <input type="radio" name="avatar" id="a1" value="img/avatares/esteves-avatar.png">
                    <label class="avatar" for="a1"><img src="img/avatares/esteves1.png" class="avt" alt="avatar Esteves" value="" ></label>

                    <input type="radio" name="avatar" id="a2" value="img/avatares/muriel-avatar.png">
                    <label class="avatar" for="a2"><img src="img/avatares/muriel1.png" class="avt" alt="avatar Muriel"></label><br>

                    <input type="radio" name="avatar" id="a3" value="img/avatares/melo-avatar.png">
                    <label class="avatar" for="a3"><img src="img/avatares/melo1.png" class="avt" alt="avatar Melo"></label>

                    <input type="radio" name="avatar" id="a4" value="img/avatares/ariel-avatar.png">
                    <label class="avatar" for="a4"><img src="img/avatares/ariel1.png" class="avt" alt="avatar Ariel"></label><br>

                    <input type="radio" name="avatar" id="a5" value="img/avatares/cesar-avatar.png">
                    <label class="avatar" for="a5"><img src="img/avatares/cesar1.png" class="avt" alt="avatar César"></label>

                    <input type="radio" name="avatar" id="a6" value="img/avatares/James-avatar.png">
                    <label class="avatar" for="a6"><img src="img/avatares/James1.png" class="avt" alt="avatar James"></label>
                </div>
            </center>
        </div>
    </div>
    <div class="row align-items-end justify-content-end">
        <div class="col-sm-4">
            <p>Já possui cadastro?<a href="">Clique aqui</a></p>
        </div>
        <div class="col-sm-2" id="enviar">
                <button type="submit" class="btn btn-primary">
                        {{ __('Registre-se') }}
                    </button>
            </form>
        </div>
    </div>
</div>

@endsection
