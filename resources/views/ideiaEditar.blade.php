@extends('layouts.app')

@section('titulo', 'Editar Ideias')

@section('content1')
@include('layouts.includes.navbarGeral')
@endsection

@section('content')
<div class="container" id="cor">
    <img src="img/titulo/atualizando.jpg" class="rounded mx-auto d-block" id="foto">
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <form method="post" action="{{ route('ideia.atualizar') }}" enctype="multipart/form-data">
                @csrf
                {{method_field('PUT')}}
                <p>
                    Título
                    <input type="text" name="titulo-ideia" class="form-control" value="{{isset($registro->titulo) ? $registro->titulo : ''}}">
                </p>
                <p>
                    Descrição
                    <input name="descricao-ideia" class="form-control" value="{{isset($registro->descricao) ? $registro->descricao : ''}}">
                </p>
                <p>
                    Benefícios
                    <input class="form-control" value="{{isset($registro->beneficio) ? $registro->beneficio : ''}}">
                </p>
                <p>
                    Adicionar Participantes
                    <input name="colaboradores" class="form-control" value="{{isset($registro->participante) ? $registro->participante : ''}}">
                </p>
                <p id="arquivo">                    
                    Adicionar arquivo em anexo<br>
                    <input type="file" value="{{isset($registro->participante) ? $registro->participante : ''}}" id="saida-anexo" name="anexo-ideia" class="file-path-wrapper">
                </p>
                <div id="enviar">
                    <a id="cancela"class="btn btn-primary mb-2" href="{{route('ideia')}}">Cancelar</a>
                    <input type="submit" id="salva" value="Salvar" class="btn btn-primary mb-2">
                    <input type="submit" id="publica" value="Publicar" class="btn btn-primary mb-2">
                </div> 
            </form>
        </div>
    </div>
</div>
@endsection
