@extends('layouts.app')

@section('titulo', 'Cadastro de Ideias')

@section('content1')
@include('layouts.includes.navbarSuper')
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center" id="cor" style="padding: 20px;">
        <div class="col-sm-6">
            <h4 class="text-primary font-weight-bold text-center">Metotodologias Disponíveis</h4>
            <table class="table text-center" id="minhaTabela1">
                <thead class="thead-light">
                    <tr>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($registros as $registro)				
					<tr>
						<td>
							<a href='#' data-toggle='modal' data-target='#metodo-modal'>{{ $registro->titulo }}</a>
						</td>
					</tr>
					@empty
					<tr>
						<td>
							<p >Não há Metodologia cadastrada!</p>
						</td>
					</tr>
					@endforelse
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" id="color">
        <div class="col-sm-6">
            <a href="{{ route('metodologianova') }}" class="btn btn-info" id="metodo">Cadastrar nova Metodologia</a>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="metodo-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{isset($registro->titulo) ? $registro->titulo : ''}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST">
                <div class="modal-body">
                    <h6>Definição</h6>
                    <p>{{isset($registro->definicao) ? $registro->definicao : ''}}</p>
                    <h6>Descrição</h6>
                    <p>{{isset($registro->descricao) ? $registro->descricao : ''}}</p>
                    <h6>Objetivo</h6>
                    <p>{{isset($registro->objetivo) ? $registro->objetivo : ''}}</p>
                    <h6>Saiba Mais</h6>
                    <p>{{isset($registro->anexo) ? $registro->anexo : ''}}</p>
                </div>
                <div class="modal-footer">
                    <a href="{{route('metodologiasuper.deletar', isset($registro->id) ? $registro->id : '') }}" class="btn btn-success">Deletar<i class="fa fa-pencil"></i></a>
                    <a href="{{route('metodologia.editar', isset($registro->id) ? $registro->id : '') }}" class="btn btn-success">Editar<i class="fa fa-pencil"></i></a>							
                </div>
            </form>
        </div>
    </div>
</div>

<!-- /Modal -->

@endsection