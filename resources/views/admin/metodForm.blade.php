@extends('layouts.app')

@section('titulo', 'Cadastro de Metodologia')

@section('content1')
@include('layouts.includes.navbarSuper')
@endsection

@section('content')
<div class="container" id="cor">
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <h1 class="text-center">Nova Metodologia</h1>
            <form name="metodologia" action="{{ route('metodologia.salvar') }}" method="POST">
                @csrf
                <p>
                    Título
                    <input type="text" class="form-control" name="titulo" required="required" placeholder="Nome da metodologia">
                </p>
                <p>
                    Definição
                    <input class="form-control" name="definicao" required="required" placeholder="Explique a metodologia">
                </p>
                <p>
                    Descrição
                    <input class="form-control" name="descricao" placeholder="Detalhes da metodologia">
                </p>
                <p>
                    Objetivo
                    <input class="form-control" name="objetivo" placeholder="Para que serve">
                </p>
                <p>
                    Anexar Exemplo
                    <input type="file" id="saida-anexo" name="anexo" multiple="multiple" class="form-control-file">
                </p>
                <div id="enviar">
                    <a href="{{ route('metodologiasuper.listar') }}" class="btn btn-primary">Voltar</a>
                    <input type="submit" value="Salvar" class="btn btn-primary">
                </div>            
            </form>            
        </div>
    </div>
</div>
@endsection