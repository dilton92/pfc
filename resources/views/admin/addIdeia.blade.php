@extends('layouts.app')

@section('titulo', 'Adicionar Ideias às Campanhas')

@section('content1')
@include('layouts.includes.navbarSuper')
@endsection

@section('content')
<div class="container" id="cor">
    <img src="img/titulo/campanha-ideia.jpg" class="rounded mx-auto d-block" id="foto">
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <form action="ideiacampanhasalvar.php" method="post">
                @csrf
                <p>
                    <label for="campanha" class="sr-only">Campanha</label>
                    <select class="form-control" id="campanha" name="campanha" placeholder="Campanha que está participando" disabled>
                        <option>Campanha x</option>
                    </select>
                </p>
                <p>
                    <label for="tema-c" class="sr-only">Tema</label>
                    <input type="text" class="form-control" id="tema-c" name="tema-c" placeholder="Tema da campanha" disabled>
                </p>
                <p>
                    <label for="titulo-ic" class="sr-only">Título</label>
                    <input type="text" class="form-control" id="titulo-ic" name="titulo-ic" placeholder="Titulo da solução proposta" required>
                </p>
                <p>
                    <label for="descricao-ic" class="sr-only">Descrição</label>
                    <textarea class="form-control" id="descricao-ic" name="descricao-ic" placeholder="Descrição da solução proposta" required></textarea>
                </p>
                <p>
                    <label for="beneficio-ic" class="sr-only">Benefícios</label>
                    <input type="text" class="form-control" id="beneficio-ic" name="beneficio-ic" placeholder="Benefícios da solução proposta" required>
                </p>
                <p>
                    <label for="anexo-ic" class="sr-only">Anexo</label>
                    <input type="file" class="form-control-file" id="anexo-ic" name="anexo-ic[]" multiple="multiple">
                </p>
                <button class="btn btn-danger" id="botao">Cancelar</button>
                <input type="submit" class=" btn btn-primary" id="botao" value="Salvar">
                <button type="submit" class="btn btn-info" id="botao">Enviar para Validação</button>	
            </form>
        </div>
    </div>
</div>
@endsection