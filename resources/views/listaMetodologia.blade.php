@extends('layouts.app')

@section('titulo', 'Metodologias Disponíveis')

@section('content1')
@include('layouts.includes.navbarGeral')
@endsection

@section('content')

<div class="container">
	<div class="row justify-content-center" id="cor" style="padding: 20px;">
		<div class="col-sm-6">
			<h4 class="text-primary font-weight-bold text-center">Metotodologias Disponíveis</h4>
			<table class="table text-center" id="minhaTabela1">
				<thead class="thead-light">
					<tr>
						<th>Nossas Metodologias</th>
					</tr>
				</thead>
				<tbody>	
					@forelse($registros as $registro)				
					<tr>
						<td>
							<a href='#' data-toggle='modal' data-target='#metodo-modal'>{{ $registro->titulo }}</a>
						</td>
					</tr>
					@empty
					<tr>
						<td>
							<p >Não há Metodologia cadastrada!</p>
						</td>
					</tr>
					@endforelse					
				</tbody>
			</table>
		</div>
	</div>
</div>
	
	<!-- Modal -->
	<div class="modal fade" id="metodo-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">{{isset($registro->titulo) ? $registro->titulo : ''}}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form>
					@csrf
					<div class="modal-body">
						<h6>Definição</h6>
						<p>{{isset($registro->definicao) ? $registro->definicao : ''}}</p>
						<h6>Descrição</h6>
						<p>{{isset($registro->definicao) ? $registro->definicao : ''}}</p>
						<h6>Objetivo</h6>
						<p>{{isset($registro->objetivo) ? $registro->objetivo : ''}}</p>
						<h6>Saiba Mais</h6>
						<p>{{isset($registro->anexo) ? $registro->anexo : ''}}</p>
					</div>
				</form>				
			</div>
		</div>
	</div>		
	<!-- /Modal -->

@endsection