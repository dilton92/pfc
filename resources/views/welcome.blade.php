@extends('layouts.layoutWelcome')


@section('content')

@include('layouts.includes.navbarWelcome')

<!-- Header -->
<header class="masthead bg-primary text-white text-center">
    <div class="container">
      <img class="img-fluid mb-5 d-block mx-auto" src="{{asset('img/logo/logo5.png')}}" width="400px" height="50px">
      <h1 class="text-uppercase mb-0">ENTROPIA</h1>
      <hr class="star-light">
      <h2 class="font-weight-light mb-0">O laboratório de inovação da sua empresa.</h2>
    </div>
  </header>

  <!-- Portfolio Grid Section -->
  <section class="portfolio" id="portfolio">
    <div class="container">
      <h2 class="text-center text-uppercase text-secondary mb-0">Avatares</h2>
      <hr class="star-dark mb-5">
      <div class="row">
        <div class="col-md-6 col-lg-4">
          <a class="portfolio-item d-block mx-auto" href="#avatar1">
            <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
              <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                <i class="fas fa-search-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid rounded mx-auto d-block" src="{{asset('img/avatares/esteves.png')}}">
          </a>
        </div>
        <div class="col-md-6 col-lg-4">
          <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-2">
            <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
              <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                <i class="fas fa-search-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid rounded mx-auto d-block" src="{{asset('img/avatares/muriel.png')}}">
          </a>
        </div>
        <div class="col-md-6 col-lg-4">
          <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-3">
            <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
              <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                <i class="fas fa-search-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid rounded mx-auto d-block" src="{{asset('img/avatares/melo.png')}}">
          </a>
        </div>
        <div class="col-md-6 col-lg-4">
          <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-4">
            <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
              <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                <i class="fas fa-search-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid rounded mx-auto d-block" src="{{asset('img/avatares/ariel.png')}}">
          </a>
        </div>
        <div class="col-md-6 col-lg-4">
          <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-5">
            <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
              <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                <i class="fas fa-search-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid rounded mx-auto d-block" src="{{asset('img/avatares/Cesar.png')}}">
          </a>
        </div>
        <div class="col-md-6 col-lg-4">
          <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-6">
            <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
              <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                <i class="fas fa-search-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid rounded mx-auto d-block" src="{{asset('img/avatares/James.png')}}">
          </a>
        </div>
      </div>
    </div>
  </section>

  <!-- Área de Descrição -->
  <section class="bg-primary text-white mb-0" id="about">
    <div class="container">
      <h2 class="text-center text-uppercase text-white">Sobre</h2>
      <hr class="star-light mb-5">
      <div class="row">
        <div class="col-lg-4 ml-auto">
          <p class="lead">Como o próprio nome já diz, o laboratório de inovação é um espaço empresarial projetado para criar condições favoráveis para que a inovação ocorra. Através do estabelecimento de um ambiente criativo e colaborativo, novos conhecimentos podem ser facilmente compartilhados e ideias podem ser desenvolvidas.</p>
        </div>
        <div class="col-lg-4 mr-auto">
          <p class="lead">Um laboratório de inovação irá servir como um espaço lúdico para adquirir uma visão holística dos desafios a serem enfrentados, permitindo que as soluções possam ser pensadas por ângulos diferentes, mantendo sempre o usuário final no centro de todo o processo.</p>
        </div>
      </div>
    </div>
  </section>
  <!-- /Área de Descrição -->

  <!-- Rodapé -->
  <footer class="footer text-center">
    <div class="container">
      <div class="row">
        <div class="col-md-4 mb-5 mb-lg-0">
          <h4 class="text-uppercase mb-4">Desenvolvedores</h4>
          <p class="lead mb-0">André Luis, Andreza Vitório, <br>Dilton Costa
            <br>Lucas Amorim, Ruan Felipe e <br>Thiago Benjamin.</p>
        </div>
        <div class="col-md-4 mb-5 mb-lg-0">
          <h4 class="text-uppercase mb-4">Ta na rede</h4>
          <ul class="list-inline mb-0">
            <li class="list-inline-item">
              <a class="btn btn-outline-light btn-social text-center rounded-circle" target="_blank" href="https://www.facebook.com/entropia.inovacoes/?modal=admin_todo_tour">
                <i class="fab fa-fw fa-facebook-f"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a class="btn btn-outline-light btn-social text-center rounded-circle" target="_blank" href="https://twitter.com/InovaEntropia">
                <i class="fab fa-fw fa-twitter"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a class="btn btn-outline-light btn-social text-center rounded-circle" target="_blank" href="https://www.instagram.com/entropia.inovacao/">
                <i class="fab fa-fw fa-instagram"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="col-md-4">
          <h4 class="text-uppercase mb-4">Sobre o sistema</h4>
          <p id="fonte"> O Entropia é um laboratório de inovação voltado para empresas e indústrias que desejam emergir seus processos e relações na indústria 4.0, com a intenção de transformar colaboradores em fiéis stakeholders, tornando o seu branding cada vez mais robusto e gerando vantagens competitivas no cenário econômico.</p>
        </div>
      </div>
    </div>
  </footer>
  <!-- /Rodapé -->

  <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
  <div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
      <i class="fa fa-chevron-up"></i>
    </a>
  </div>

  @include('layouts.includes.modaisWelcome')

@endsection

