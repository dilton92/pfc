@extends('layouts.app')

@section('content')


@section('titulo', 'Bem Vindo ao Entropia')

@section('content1')
@include('layouts.includes.navbarGeral')
@endsection

@section('content')

<body>
    <div class="container-fluid gedf-wrapper">
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ Auth::user()->avatar }}" class="rounded mx-auto d-block">
                        @if (session('status'))
                        <div class="h5">
                            @ {{ session('status') }}
                        </div>
                        @endif
                        <div class="h7 text-muted">
                            Nome Completo: {{ Auth::user()->name }}

                        </div>
                        <hr>
                        <img src="img/moeda.png">
                        <div class="h7 text-muted">100 pontos</div>
                    </div>
                </div>
                <br>

                <!-- Ranking -->
                <div class="card gedf-card" id="rankin-colab">
                    <div class="card-body" id="rank">
                        <h5 class="card-title">Ranking dos Colaboradores</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Interaja e suba de posição</h6>

                        <table class="d-flex justify-content-between align-items-center" id="tabela1">
                            <tr>
                                <td class="e">1º</td>
                                <td class="d">@Amorim</td>
                            </tr>
                            <tr>
                                <td class="e">2º</td>
                                <td class="d">@Vitório</td>
                            </tr>
                            <tr>
                                <td class="e">3º</td>
                                <td class="d">@Benjamin</td>
                            </tr>
                            <tr>
                                <td class="e">4º</td>
                                <td class="d">@Dilton</td>
                            </tr>
                            <tr>
                                <td class="e">5º</td>
                                <td class="d">@Felipe</td>
                            </tr>
                            <tr>
                                <td class="e">6º</td>
                                <td class="d">@André</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Ranking -->

            <div class="col-md-6 gedf-main">
                <!-- Carousel -->
                <div class="card gedf-card">
                    <div class="card-body">
                        <div id="carouselCampanha" class="carousel slide" data-ride="carousel">

                            <div class="carousel-inner">
                                @foreach($ideias->take(3) as $ideia)     
                                    @if($ideia->tipo_ideia == 1)                       
                                        <div class="carousel-item @if($loop->index ) active @endif">                                        
                                            <img src="{{ asset($ideia->anexo)}}" class="img-fluid d-block w-100">
                                            <div class="carousel-caption d-none d-md-block">
                                                <h3>{{$ideia->titulo}}</h3>
                                                <p>{{$ideia->descricao}}</p>
                                                <a class="btn btn-info label label-primary" href="{{ route('ideia') }}">Mande sua Ideia</a>
                                            </div>                                    
                                        </div>
                                    @endif
                                @endforeach  
                            </div>

                            <a class="carousel-control-prev" href="#carouselCampanha" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Anterior</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselCampanha" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Próximo</span>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /Carousel -->                
                <br>
                <!-- Post -->
                <div class="card gedf-card">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="posts-tab" data-toggle="tab" role="tab" aria-controls="posts" aria-selected="true">Faça uma Publicação</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('postar') }}" method="POST">
                            @csrf
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">                                    
                                    <div class="form-group">
                                        <input type="hidden" name="titulo" value="null">                                        
                                        <label class="sr-only" for="message"></label>
                                        <textarea class="form-control" name="descricao" id="descricao" placeholder="Interaja com seus colegas de trabalho..."></textarea>
                                        <input type="hidden" name="beneficio" value="null">
                                        <input type="hidden" name="participante" value="null">
                                        <input type="hidden" name="id_user" value="{{Auth::user()->id}}">
                                        <input type="hidden" name="tipo_ideia" value="2">
                                    </div>                                    
                                </div>
                                <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                                    <div class="form-group"></div>
                                </div>
                            </div>
                            <div class="btn-toolbar justify-content-between">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-primary">Publicar</button>
                                </div>                            
                            </div>
                        </form>
                    </div>
                </div>
                
                <!-- /Post -->
                <br>
                <!-- Post -->
                @foreach($ideias as $ideia)
                <div class="card gedf-card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="45" src="" alt="">
                                </div>
                                <div class="ml-2">
                                    <div class="h5 m-0">{{ $ideia->user['nomeusuario'] }}</div>
                                    <div class="h7 text-muted"></div>
                                </div>
                            </div>
                            <div>
                                <div class="dropdown">
                                    <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="text-muted h7 mb-2"> <i class="fa fa-clock-o"></i>{{date("D/m/y", strtotime($ideia->updated_at))}}</div>
                        
                        <p class="card-text">{{$ideia->descricao}}</p>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="card-link"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Curtir</a>
                        <a class="card-link" onclick="mostrar('a')"><i class="fa fa-comment"></i>Comentar</a>
                    </div>

                    <div class="hidden" id="a">
                        <form class="search" method="post" action="addfeed.php">
                            <input type="text" class="form-control" id="comentario" name="comentario" placeholder="Escreva seu comentário..." required>
                            <input id="comentar" type="submit" value="Comentar" class="btn btn-primary btn-sm">
                        </form>
                    </div>

                </div>
                <br>
                @endforeach
                <!-- /Post -->

                <!-- Post -->
                <div class="card gedf-card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="45" src="img/logo/logo1.png" alt="">
                                </div>
                                <div class="ml-2">
                                    <div class="h5 m-0">ENTROPIA</div>
                                </div>
                            </div>
                            <div>
                                <div class="dropdown">
                                    <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <h5 class="card-title">BEM-VINDO!!!</h5>
                        <p class="card-text">
                            SEJA BEM-VINDO AO SEU LABORATÓRIO DE INOVAÇÃO PREFERIDO!
                        </p>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="card-link"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Curtir</a>
                    </div>
                </div>
                
            </div> 

            <!-- Ranking -->
            <div class="col-md-3">
                <div class="card" id="rankin-ideia">
                    <div class="card-body" id="rankin">
                        <h5 class="card-title">Ranking de Ideias</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Seja criativo e suba de posição</h6>
                        <table class="d-flex justify-content-between align-items-center" id="tabela">
                            <tr>
                                <td class="esquerdo">1º</td>
                                <td class="direito">Ideia E</td>
                            </tr>
                            <tr>
                                <td class="esquerdo">2º</td>
                                <td class="direito">Ideia Q</td>
                            </tr>
                            <tr>
                                <td class="esquerdo">3º</td>
                                <td class="direito">Ideia O</td>
                            </tr>
                            <tr>
                                <td class="esquerdo">4º</td>
                                <td class="direito">Ideia P</td>
                            </tr>
                            <tr>
                                <td class="esquerdo">5º</td>
                                <td class="direito">Ideia A</td>
                            </tr>
                            <tr>
                                <td class="esquerdo">6º</td>
                                <td class="direito">Ideia Y</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <!-- /Ranking -->

        </div>
    </div>
    @endsection