@extends('layouts.app')

@section('titulo', 'Cadastro de Ideias')

@section('content1')
@include('layouts.includes.navbarGeral')
@endsection

@section('content')

<div class="container" id="cor">
        <img src="img/titulo/ideia.jpg" class="rounded mx-auto d-block" id="foto">
        <div class="row justify-content-center">
       		<div class="col-sm-4">
				<table id="datatable" class="table text-center">
					<thead class="thead-light edit">
						<tr>
							<th scope="col">Ideias salvas por você</th>
						</tr>
					</thead>
					@foreach($registros as $registro)
						@if($registro->tipo_ideia == 1)																		
							<tr >						
								<td>
									<form action="{{route('ideia.editar', isset($registro->id) ? $registro->id : '0') }}">
										<a class="edit" href="#modal" id='linha' data-toggle='modal' data-target="#listModal">{{ $registro->titulo}}</a>								
									</form>
								</td>						
							</tr>					
						@endif
					@endforeach					
				</table>
			</div>
			<div class="col-sm-1"></div>
			<div class="col-sm-6">
				<aside>
					<h4 class="text-center">Mande uma nova Ideia</h4>
					<form method="POST" action="{{ route('ideia.salvar') }}" enctype="multipart/form-data">
						@csrf
						<p>
							Título
							<input type="text" name="titulo" required="required" placeholder="Dê um nome para sua ideia" class="form-control">
						</p>
						<p>
							Descrição
							<textarea name="descricao" required="required" placeholder="Detalhamento da ideia" class="form-control"></textarea>
						</p>
						<p>
							Benefícios
							<textarea name="beneficio" required="required" placeholder="Quais melhorias trará para a empresa?" class="form-control"></textarea>
						</p>
						<p>
							Adicionar Participantes
							<textarea name="participante" class="form-control"></textarea>
						</p>
						<p id="arquivo">
							Adicionar arquivo em anexo<br>
							<input type="file" id="saida-anexo" name="anexo" multiple="multiple" class="form-control-file">
						</p>
						<input type="hidden" name="id_user" value="{{Auth::user()->id}}">
						<input type="hidden" name="tipo_ideia" value="1">
						<div id="enviar">
							<a id="cancela"class="btn btn-primary mb-2" href="{{route('home')}}">Cancelar</a>
							<input type="submit" id="publica" value="Publicar" class="btn btn-primary mb-2">
						</div>
					</form>
				</aside>
       		</div>
        </div>
	</div>



	<!-- Modal -->
	<div class="modal fade" id="listModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="titulo">{{isset($registro->titulo) ? $registro->titulo : ''}}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form id="listForm" method="POST">
					<div class="modal-body">
						<h6>Id</h6>
						<input id="id" value="{{isset($registro->id) ? $registro->id : ''}}" >
						<h6>Descrição</h6>
						<p id="descricao"></p>						
						<h6>Benefícios</h6>
						<p id="beneficios">{{isset($registro->beneficio) ? $registro->beneficio : ''}}</p>
						<h6>Participantes</h6>
						<p id="participantes">{{isset($registro->participante) ? $registro->participante : ''}}</p>
					</div>
					<div class="modal-footer">
						<a href="{{route('ideia.deletar', isset($registro->id) ? $registro->id : '0') }}" class="btn btn-success">Deletar<i class="fa fa-pencil"></i></a>
						<a href="{{route('ideia.editar', isset($registro->id) ? $registro->id : '0') }}" class="btn btn-success">Editar<i class="fa fa-pencil"></i></a>							
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /Modal -->


@endsection
