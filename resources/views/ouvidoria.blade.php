@extends('layouts.app')

@section('titulo', 'Fale Conosco')

@section('content1')
@include('layouts.includes.navbarGeral')
@endsection

@section('content')
<div id="cor" class="container">
    <img src="img/titulo/sugestao.jpg" class="rounded mx-auto d-block">
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <form method="post" action="{{ route('ouvidoria.salvar') }}">
                @csrf
                <p>
                    Assunto
                    <select name="assunto" class="form-control" id="assunto">                        
                        <option value="sugestao">Sugestão</option>
                        <option value="reclamacao">Reclamação</option>
                        <option value="critica">Crítica</option>
                        <option value="elogio">Elogio</option>
                        <option value="solicitacao">Solicitação</option>
                        <option value="denuncia">Denúncia</option>
                    </select>
                </p>
                <p>
                    Setor
                    <select name="setor" class="form-control" id="setor-sug">                        
                        <option value="administrativo">Administrativo</option>
                        <option value="financeiro">Financeiro</option>
                        <option value="rh">Recursos Humanos</option>
                        <option value="comercial">Comercial</option>
                        <option value="operacional">Operacional</option>
                    </select>
                </p>
                <p>
                    Mensagem
                    <input name="mensagem" required="required"  placeholder="Comentário..." class="form-control">
                </p>
                <p id="arquivo">
                    Adicionar arquivo em anexo
                    <input type="file" id="saida-anexo" name="anexo-projeto" class="form-control-file">
                </p>
                <div id="enviar">
                    <a name="cancelar" href="{{ route('home') }}" class="btn btn-primary">Cancelar</a>
                    <input type="submit" id="salva" value="enviar" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection