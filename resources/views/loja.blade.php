@extends('layouts.app')

@section('titulo', 'Loja')

@section('content1')
@include('layouts.includes.navbarGeral')
@endsection

@section('content')
<center>
    <div class="card-deck">
        <div class="card text-center" style="width: 18rem;">
            <img class="card-img-top" src="img/logo/logo1.1.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Produto 1</h5>
                <p class="card-text">100 pontos</p>
                <a href="#" class="btn btn-primary">Trocar</a>
            </div>
        </div>

        <div class="card text-center" style="width: 18rem;">
            <img class="card-img-top" src="img/game.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Produto 2</h5>
                <p class="card-text">100 pontos</p>
                <a href="#" class="btn btn-primary">Trocar</a>
            </div>
        </div>

        <div class="card text-center" style="width: 18rem;">
            <img class="card-img-top" src="img/logo/logo1.1.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Produto 3</h5>
                <p class="card-text">100 pontos</p>
                <a href="#" class="btn btn-primary">Trocar</a>
            </div>
        </div>

        <div class="card text-center" style="width: 18rem;">
            <img class="card-img-top" src="img/game.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Produto 4</h5>
                <p class="card-text">100 pontos</p>
                <a href="#" class="btn btn-primary">Trocar</a>
            </div>
        </div>
    </div>
    <br>
    <div class="card-deck">
        <div class="card text-center" style="width: 18rem;">
            <img class="card-img-top" src="img/logo/logo1.1.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Produto 5</h5>
                <p class="card-text">100 pontos</p>
                <a href="#" class="btn btn-primary">Trocar</a>
            </div>
        </div>

        <div class="card text-center" style="width: 18rem;">
            <img class="card-img-top" src="img/game.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Produto 6</h5>
                <p class="card-text">100 pontos</p>
                <a href="#" class="btn btn-primary">Trocar</a>
            </div>
        </div>

        <div class="card text-center" style="width: 18rem;">
            <img class="card-img-top" src="img/logo/logo1.1.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Produto 7</h5>
                <p class="card-text">100 pontos</p>
                <a href="#" class="btn btn-primary">Trocar</a>
            </div>
        </div>
        <div class="card text-center" style="width: 18rem;">
            <img class="card-img-top" src="img/game.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Produto 8</h5>
                <p class="card-text">100 pontos</p>
                <a href="#" class="btn btn-primary">Trocar</a>
            </div>
        </div>
    </div>
</center>
@endsection